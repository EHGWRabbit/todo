import React, { Component } from 'react';
import axios from 'axios';

class App extends Component{
  state = {
    todo: []
  };
  componentDidMount(){
    this.getTodo();
  }
  getTodo(){
    axios
    .get('http://127.0.01:8000/api/')
    .then(res => {
      this.setState({ todo: res.data });
    })
    .catch(err => {
      console.log(err);
    });
  }
  render(){
    return(
      <div>
        {this.state.todo.map(item => (
          <div key = {item.id}>
            <h1> {item.title}</h1>
            <span>{item.body}</span>
          </div>
        ))}
      </div>
    );
  }
}
  
export default App;
